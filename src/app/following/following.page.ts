import { Board } from './../models/board.interface';
import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { DatabaseUser } from '../models/databaseUser.interface';
import { User } from '../models/user.interface';
import { AuthenticationService } from '../services/authentication.service';
import { BoardsService } from '../services/boards.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-following',
  templateUrl: './following.page.html',
  styleUrls: ['./following.page.scss'],
})
export class FollowingPage implements OnInit {
  public dbUser: DatabaseUser;
  public user$: User;
  public boards: Board[] = [];

  constructor(
    private userService: UsersService,
    private authService: AuthenticationService,
    private boardService: BoardsService,
    private toastController: ToastController,
    private loadingController: LoadingController
  ) {
    this.getUser();
    setTimeout(() => {
      this.fetchData();
    }, 3000);
    this.showLoading();
  }

  ngOnInit() {}

  private async getUser() {
    await this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
        this.userService.getUserFromDatabase(this.user$).then((dbUser) => {
          if (dbUser) {
            this.dbUser = dbUser;
          } else {
            this.dbUser = null;
          }
        });
      } else {
        this.user$ = null;
      }
    });
  }

  private fetchData() {
    for (const boardId of this.dbUser.boardsFollowing) {
      this.boardService.getBoardById(boardId).then((board) => {
        this.boards.push(board);
      });
    }
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Loading data...',
      duration: 3000,
    });

    await loading.present();
  }
}
