import { PostShort } from './post-short.interface';
import { User } from './user.interface';
export interface DatabaseUser extends User {
    posts: PostShort[];
    boardsFollowing: string[];
}