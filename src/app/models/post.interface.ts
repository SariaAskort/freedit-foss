import { Comment } from "./comment.interface";

export interface Post {
    id: string;
    title: string;
    body: string;
    imageUrl: string;
    authorUid: string;
    authorImageUrl: string;
    authorName: string;
    comments: Comment[];
    karma: number;
    votedByUid: string[];
    creationDate: Date;
    boardId: string;
}