export interface Comment {
    uuid: string;
    authorUid: string;
    authorImageUrl: string;
    authorEmail: string;
    body: string;
    karma: number;
    votedByUid: string[];
    responses?: Comment[];
}