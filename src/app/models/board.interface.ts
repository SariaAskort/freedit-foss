import { PostShort } from './post-short.interface';
export interface Board {
    id: string;
    handle: string;
    name: string;
    description: string;
    followers: string[];
    posts: PostShort[];
    adminUid: string;
    moderatorsUids: string[];
    isAnonymous: boolean;
    imageUrl: string;
    lastModified: Date;
}