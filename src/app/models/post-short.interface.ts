export interface PostShort {
    id: string;
    title: string;
    imageUrl: string;
}