import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import {
  PreloadAllModules,
  RouterModule,
  Routes,
  CanActivate,
} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'boards',
    pathMatch: 'full',
  },
  {
    path: 'boards',
    loadChildren: () =>
      import('./boards/boards/boards.module').then((m) => m.BoardsPageModule),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./authentication/login/login.module').then(
        (m) => m.LoginPageModule
      ),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./authentication/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },
  {
    path: 'verify-email',
    loadChildren: () =>
      import('./authentication/verify-email/verify-email.module').then(
        (m) => m.VerifyEmailPageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'forgot-password',
    loadChildren: () =>
      import('./authentication/forgot-password/forgot-password.module').then(
        (m) => m.ForgotPasswordPageModule
      ),
  },
  {
    path: 'logout',
    loadChildren: () =>
      import('./authentication/logout/logout.module').then(
        (m) => m.LogoutPageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'posts',
    loadChildren: () =>
      import('./posts/posts/posts.module').then((m) => m.PostsPageModule),
  },
  {
    path: 'following',
    loadChildren: () =>
      import('./following/following.module').then((m) => m.FollowingPageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'create-board',
    loadChildren: () =>
      import('./boards/create-board/create-board.module').then(
        (m) => m.CreateBoardPageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'board/:idBoard',
    loadChildren: () =>
      import('./boards/board/board.module').then((m) => m.BoardPageModule),
  },
  {
    path: 'anon-auth',
    loadChildren: () =>
      import('./authentication/anon-auth/anon-auth.module').then(
        (m) => m.AnonAuthPageModule
      ),
  },
  {
    path: 'board/:idBoard/post/:idPost',
    loadChildren: () =>
      import('./posts/post/post.module').then((m) => m.PostPageModule),
  },
  {
    path: 'board/:idBoard/create-post',
    loadChildren: () =>
      import('./posts/create-post/create-post.module').then(
        (m) => m.CreatePostPageModule
      ),
  },
  {
    path: 'board/:idBoard/post/:idPost/create-comment/:idComment',
    loadChildren: () =>
      import('./posts/create-comment/create-comment.module').then(
        (m) => m.CreateCommentPageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'board/:idBoard/post/:idPost/create-comment',
    loadChildren: () =>
      import('./posts/create-comment/create-comment.module').then(
        (m) => m.CreateCommentPageModule
      ),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
