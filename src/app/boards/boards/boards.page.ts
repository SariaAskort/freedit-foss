import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll, LoadingController } from '@ionic/angular';
import { Board } from 'src/app/models/board.interface';
import { User } from 'src/app/models/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BoardsService } from 'src/app/services/boards.service';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.page.html',
  styleUrls: ['./boards.page.scss'],
})
export class BoardsPage implements OnInit {
  public boards: Board[];
  public user$: User;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private boardService: BoardsService,
    private authService: AuthenticationService,
    private loadingController: LoadingController
  ) {
    const page = this.boardService.getPaginatedBoards();
    this.boards = page;
    this.getUser();
    this.showLoading();
  }

  ngOnInit() {}

  public getMoreBoards() {
    const lastBoard = this.boards[this.boards.length - 1];
    const page = this.boardService.getPaginatedBoards(lastBoard);
    this.boards.concat(page);
  }

  private getUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
      } else {
        this.user$ = null;
      }
    });
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Loading data...',
      duration: 1000,
    });

    await loading.present();
  }
}
