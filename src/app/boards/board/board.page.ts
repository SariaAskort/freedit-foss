import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Board } from 'src/app/models/board.interface';
import { DatabaseUser } from 'src/app/models/databaseUser.interface';
import { User } from 'src/app/models/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BoardsService } from 'src/app/services/boards.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.page.html',
  styleUrls: ['./board.page.scss'],
})
export class BoardPage implements OnInit {
  public board: Board;
  public user$: User;
  public dbUser: DatabaseUser;
  private id: string;

  constructor(
    private route: ActivatedRoute,
    private boardsService: BoardsService,
    private loadingController: LoadingController,
    private authService: AuthenticationService,
    private userService: UsersService
  ) {
    this.showLoading();
    this.getUser();
    this.fetchData();
  }

  ngOnInit() {}

  private async fetchData() {
    this.route.params.subscribe((params) => {
      if (params['idBoard']) {
        this.id = params['idBoard'];
        this.boardsService.getBoardById(this.id).then((board) => {
          this.board = board;
        });
      }
    });
  }

  public followBoard() {
    if (this.dbUser) {
      this.dbUser.boardsFollowing.push(this.board.id);
      this.userService.updateUser(this.dbUser);
    }
  }

  public unfollowBoard() {
    this.dbUser.boardsFollowing = this.dbUser.boardsFollowing.filter((id) => {
      return id !== this.board.id;
    });

    this.userService.updateUser(this.dbUser);
  }

  public isFollowing() {
    for (const id of this.dbUser.boardsFollowing) {
      if (id == this.board.id) {
        return true;
      }
    }
    return false;
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Loading data...',
      duration: 1000,
    });

    await loading.present();
  }

  private getUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
        this.userService.getUserFromDatabase(this.user$).then((dbUser) => {
          this.dbUser = dbUser;
        });
      } else {
        this.user$ = null;
      }
    });
  }
}
