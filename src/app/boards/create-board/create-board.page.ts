import { LoadingController, ToastController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BoardsService } from 'src/app/services/boards.service';
import { User } from 'src/app/models/user.interface';
import { Board } from 'src/app/models/board.interface';

@Component({
  selector: 'app-create-board',
  templateUrl: './create-board.page.html',
  styleUrls: ['./create-board.page.scss'],
})
export class CreateBoardPage implements OnInit {
  private user: User;

  public handle: string;
  public name: string;
  public isAnonymous: boolean = false;
  public description: string;
  public image: File;

  constructor(
    private boardService: BoardsService,
    private authService: AuthenticationService,
    private router: Router,
    private toastController: ToastController,
    private loadingController: LoadingController
  ) {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user = user;
      }
    });
  }

  ngOnInit() {}

  public imageChange($event) {
    const max_size = 2097152;
    if (!this.checkImage($event.target.files[0])) {
      this.presentToast(
        'Filetype not allowed, only allowed .jpg, .jpeg, .png and .gif'
      );
      return false;
    }
    if ($event.target.files[0].size > max_size) {
      this.presentToast('File is too large');
      return false;
    }
    this.image = $event.target.files[0];
  }

  private checkImage($image) {
    const allowed_types = ['image/jpg', 'image/png', 'image/jpeg', 'image/gif'];
    for (const type of allowed_types) {
      if (type === $image.type) {
        return true;
      }
    }
    return false;
  }

  public async onCreate() {
    if (this.handle && this.name && this.description) {
      const board: Board = {
        handle: this.handle,
        name: this.name,
        description: this.description,
        adminUid: this.user.uid,
        imageUrl: null,
        id: null,
        followers: [],
        posts: [],
        moderatorsUids: [this.user.uid],
        isAnonymous: this.isAnonymous,
        lastModified: new Date(),
      };
      try {
        if (this.image) {
          await this.boardService.createBoard(board, this.image);
        } else {
          await this.boardService.createBoard(board);
        }
        this.showLoading();
        setTimeout(() => {
          this.router.navigate(['/boards']);
        }, 5000);
      } catch (error) {
        this.presentToast(error.message);
      }
    }
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Uploading data...',
      duration: 1500,
    });

    await loading.present();
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
