import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  public email: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {}

  public async onForgotPassword() {
    try {
      await this.authService.resetPassword(this.email);
      this.router.navigate(['/login']);
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
