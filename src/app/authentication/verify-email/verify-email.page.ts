import { Observable } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/user.interface';
import firebase from 'firebase/app';
import 'firebase/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.page.html',
  styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage implements OnInit, OnDestroy {
  public user$: User;

  constructor(
    private authService: AuthenticationService,
    private toastController: ToastController,
    private router: Router
  ) {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.user$ = user;
        if (user.emailVerified) {
          this.router.navigate(['/boards']);
        } else {
          user.sendEmailVerification();
          setInterval(() => {
            user.reload();
            if (user.emailVerified) {
              this.router.navigate(['/boards']);
            }
          }, 5000);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.authService.logout();
  }

  ngOnInit() {}

  public async onSendEmail(): Promise<void> {
    try {
      await this.authService.sendVerificationEmail();
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
