import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public displayName: string;
  public email: string;
  public pass1: string;
  public pass2: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {}

  public async onRegister() {
    try {
      if (this.pass1 === this.pass2) {
        const user = await this.authService.register(this.displayName, this.email, this.pass1);
        if (user) {
          const isVerified = this.authService.isEmailVerified(user);
          this.redirectUser(isVerified);
        }
      } else {
        this.presentToast("Password don't match.")
      }
    } catch (error) {
      this.presentToast(error.message)
    }
  }

  private redirectUser(isVerified: boolean) {
    if (isVerified) {
      this.router.navigate(['boards']);
    } else {
      this.router.navigate(['verify-email']);
    }
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
