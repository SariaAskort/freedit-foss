import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-anon-auth',
  templateUrl: './anon-auth.page.html',
  styleUrls: ['./anon-auth.page.scss'],
})
export class AnonAuthPage implements OnInit {
  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {}

  public onLoginAnonymously() {
    this.authService.signInAnonymously();

    this.router.navigate(['/boards']);
  }
}
