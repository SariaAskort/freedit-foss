import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnonAuthPageRoutingModule } from './anon-auth-routing.module';

import { AnonAuthPage } from './anon-auth.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnonAuthPageRoutingModule
  ],
  declarations: [AnonAuthPage]
})
export class AnonAuthPageModule {}
