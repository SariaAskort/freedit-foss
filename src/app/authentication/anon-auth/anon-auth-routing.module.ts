import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnonAuthPage } from './anon-auth.page';

const routes: Routes = [
  {
    path: '',
    component: AnonAuthPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnonAuthPageRoutingModule {}
