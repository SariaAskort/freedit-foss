import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/models/post.interface';
import { Comment } from 'src/app/models/comment.interface';
import { v4 as uuidv4 } from 'uuid';
import { Board } from 'src/app/models/board.interface';
import { BoardsService } from 'src/app/services/boards.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.page.html',
  styleUrls: ['./create-comment.page.scss'],
})
export class CreateCommentPage implements OnInit {
  public body: string;
  public post: Post;
  public board: Board;
  public user$: User;
  private idComment: string = null;

  constructor(
    private authService: AuthenticationService,
    private postService: PostsService,
    private boardService: BoardsService,
    private route: ActivatedRoute,
    private router: Router,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {
    this.getUser();
    this.fetchData();
  }

  public onCreate() {
    const comment: Comment = {
      uuid: uuidv4(),
      authorUid: this.board.isAnonymous ? '' : this.user$.uid,
      authorEmail: this.board.isAnonymous ? 'Anonymous' : this.user$.displayName,
      authorImageUrl: this.board.isAnonymous
        ? 'https://firebasestorage.googleapis.com/v0/b/freedit-b04a6.appspot.com/o/users%2Favatar.png?alt=media&token=286e30fb-2e8b-4a05-b623-01250a72db66'
        : this.user$.photoURL,
      body: this.body,
      karma: this.board.isAnonymous ? 0 : 1,
      votedByUid: this.board.isAnonymous ? [] : [this.user$.uid],
    };
    if (this.idComment) {
      let parent = this.post.comments.filter(
        (comment) => comment.uuid === this.idComment
      )[0];
      parent.responses
        ? parent.responses.push(comment)
        : (parent.responses = [comment]);
    } else {
      comment.responses = [];
      this.post.comments
        ? this.post.comments.push(comment)
        : (this.post.comments = [comment]);
    }
    this.postService.updatePost(this.post);
    this.showLoading();

    setTimeout(() => {
      this.router.navigate([`/board/${this.board.id}/post/${this.post.id}`]);
    }, 1500);
  }

  private async fetchData() {
    this.route.params.subscribe((params) => {
      if (params['idPost']) {
        const id = params['idPost'];
        this.postService.getPostById(id).then((post) => {
          this.post = post;
        });
      }
      if (params['idBoard']) {
        const idBoard = params['idBoard'];
        this.boardService.getBoardById(idBoard).then((board) => {
          this.board = board;
        });
      }
      if (params['idComment']) {
        this.idComment = params['idComment'];
      }
    });
  }

  private getUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
      } else {
        this.user$ = null;
      }
    });
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Uploading data...',
      duration: 1500,
    });

    await loading.present();
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }

  ngOnInit() {}
}
