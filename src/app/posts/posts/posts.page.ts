import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Post } from 'src/app/models/post.interface';
import { User } from 'src/app/models/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit {
  public posts: Post[];
  private user$: User;

  constructor(
    private postService: PostsService,
    private authService: AuthenticationService,
    private loadingController: LoadingController
  ) {
    const page = this.postService.getLatestPostsPaginated();
    this.posts = page;
    this.getUser();
    this.showLoading();
  }

  public upvote(id: string) {
    let post: Post = this.searchPost(id);
    if (post) {
      if (!this.checkVoted(post)) {
        post.karma++;
        post.votedByUid.push(this.user$.uid);
        this.postService.updatePost(post);
      }
    }
  }

  public downvote(id: string) {
    let post: Post = this.searchPost(id);
    if (post) {
      if (!this.checkVoted(post)) {
        post.karma--;
        post.votedByUid.push(this.user$.uid);
        this.postService.updatePost(post);
      }
    }
  }

  private searchPost(id: string): Post {
    for (const post of this.posts) {
      if (post.id === id) {
        return post;
      }
    }
    return null;
  }

  public checkVoted(post: Post): boolean {
    let voted: boolean = false;
    post.votedByUid.filter((uid) => {
      if (uid === this.user$.uid) {
        voted = true;
      }
    });
    return voted;
  }

  private getUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
      } else {
        this.user$ = null;
      }
    });
  }

  public getMorePosts() {
    const lastPost = this.posts[this.posts.length - 1];
    const page = this.postService.getLatestPostsPaginated(lastPost);
    this.posts.concat(page);
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Loading data...',
      duration: 1000,
    });

    await loading.present();
  }

  ngOnInit() {}
}
