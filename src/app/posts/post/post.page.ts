import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Board } from 'src/app/models/board.interface';
import { Post } from 'src/app/models/post.interface';
import { User } from 'src/app/models/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BoardsService } from 'src/app/services/boards.service';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {
  public user$: User;
  public post: Post;
  public board: Board;
  private id: string;

  constructor(
    private postService: PostsService,
    private authService: AuthenticationService,
    private boardService: BoardsService,
    private route: ActivatedRoute,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {
    this.fetchData();
    this.getUser();
    this.showLoading();
  }

  ngOnInit() {}

  private async fetchData() {
    this.route.params.subscribe((params) => {
      if (params['idPost']) {
        this.id = params['idPost'];
        this.postService.getPostById(this.id).then((post) => {
          this.post = post;
        });
      }
      if (params['idBoard']) {
        const idBoard = params['idBoard'];
        this.boardService.getBoardById(idBoard).then((board) => {
          this.board = board;
        });
      }
    });
  }

  public upvote() {
    if (!this.checkVoted()) {
      this.post.votedByUid.push(this.user$.uid);
      this.post.karma++;

      this.postService.updatePost(this.post);
    } else {
      this.presentToast('Already voted');
    }
  }

  public downvote() {
    if (!this.checkVoted()) {
      this.post.votedByUid.push(this.user$.uid);
      this.post.karma--;

      this.postService.updatePost(this.post);
    } else {
      this.presentToast('Already voted');
    }
  }

  private checkVoted(): boolean {
    let voted: boolean = false;
    this.post.votedByUid.filter((uid) => {
      if (uid === this.user$.uid) {
        voted = true;
      }
    });
    return voted;
  }

  private getUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
      } else {
        this.user$ = null;
      }
    });
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Loading data...',
      duration: 1000,
    });

    await loading.present();
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
