import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Board } from 'src/app/models/board.interface';
import { Post } from 'src/app/models/post.interface';
import { User } from 'src/app/models/user.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BoardsService } from 'src/app/services/boards.service';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.page.html',
  styleUrls: ['./create-post.page.scss'],
})
export class CreatePostPage implements OnInit {
  public board: Board;
  public user$: User;
  private id: string;

  public image: File;
  public title: string;
  public body: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private boardsService: BoardsService,
    private loadingController: LoadingController,
    private authService: AuthenticationService,
    private postService: PostsService,
    private toastController: ToastController
  ) {
    this.fetchData();
    this.getUser();
  }

  ngOnInit() {}

  public onCreate() {
    if (this.title && this.body && this.user$) {
      const post: Post = {
        id: null,
        title: this.title,
        body: this.body,
        imageUrl: null,
        authorUid: this.board.isAnonymous ? '' : this.user$.uid,
        authorImageUrl: this.board.isAnonymous
          ? 'https://firebasestorage.googleapis.com/v0/b/freedit-b04a6.appspot.com/o/users%2Favatar.png?alt=media&token=286e30fb-2e8b-4a05-b623-01250a72db66'
          : this.user$.photoURL,
        authorName: this.board.isAnonymous
          ? 'Anonymous'
          : this.user$.displayName,
        comments: [],
        karma: 1,
        votedByUid: [this.user$.uid],
        creationDate: new Date(),
        boardId: this.board.id,
      };
      try {
        if (this.image) {
          this.postService.createPost(post, this.board, this.image);
        } else {
          this.postService.createPost(post, this.board);
        }
        this.showLoading();
        setTimeout(() => {
          this.router.navigate([`/board/${this.board.id}`]);
        }, 2000);
      } catch (error) {
        this.presentToast(error.message);
      }
    }
  }

  public imageChange($event) {
    const max_size = 2097152;
    if (!this.checkImage($event.target.files[0])) {
      this.presentToast(
        'Filetype not allowed, only allowed .jpg, .jpeg, .png and .gif'
      );
      return false;
    }
    if ($event.target.files[0].size > max_size) {
      this.presentToast('File is too large');
      return false;
    }
    this.image = $event.target.files[0];
  }

  private checkImage($image) {
    const allowed_types = ['image/jpg', 'image/png', 'image/jpeg', 'image/gif'];
    for (const type of allowed_types) {
      if (type === $image.type) {
        return true;
      }
    }
    return false;
  }

  private async fetchData() {
    this.route.params.subscribe((params) => {
      if (params['idBoard']) {
        this.id = params['idBoard'];
        this.boardsService.getBoardById(this.id).then((board) => {
          this.board = board;
        });
      }
    });
  }

  private getUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
      } else {
        this.user$ = null;
      }
    });
  }

  private async showLoading() {
    let loading = await this.loadingController.create({
      message: 'Uploading data...',
      duration: 2000,
    });

    await loading.present();
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
