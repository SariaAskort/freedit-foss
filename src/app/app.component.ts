import { DatabaseUser } from './models/databaseUser.interface';
import { Observable } from 'rxjs';
import { AuthenticationService } from './services/authentication.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './models/user.interface';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public user$: User;
  private currentPage: string;

  public appPages = [
    { title: 'Latest boards', url: '/boards', icon: 'earth' },
    { title: 'Latest posts', url: '/posts', icon: 'document' },
    { title: 'Following', url: '/following', icon: 'bookmark' },
  ];
  public notLoggedIn = { title: 'Login', url: '/login', icon: 'log-in' };
  public LoggedIn = { title: 'Logout', url: '/logout', icon: 'log-out' };

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.currentPage = router.url;

    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user$ = user;
      } else {
        this.user$ = null;
      }
    });
  }
}
