import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { User } from '../models/user.interface';
import { DatabaseUser } from '../models/databaseUser.interface';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private user$: User;
  private dbUser: DatabaseUser;
  private db: firebase.firestore.Firestore;

  private userConverter = {
    toFirestore: (dbUser: DatabaseUser) => {
      return {
        uid: dbUser.uid,
        email: dbUser.email,
        displayName: dbUser.displayName,
        emailVerified: dbUser.emailVerified,
        isAnonymous: dbUser.isAnonymous,
        photoURL: dbUser.photoURL,
        posts: dbUser.posts,
        boardsFollowing: dbUser.boardsFollowing,
      };
    },
    fromFirestore: (snapshot, options) => {
      const data = snapshot.data(options);
      return {
        uid: data.uid,
        email: data.email,
        displayName: data.displayName,
        emailVerified: data.emailVerified,
        isAnonymous: data.isAnonymous,
        photoURL: data.photoURL,
        posts: data.posts,
        boardsFollowing: data.boardsFollowing,
      };
    },
  };

  constructor() {
    this.db = firebase.firestore();
  }

  public async addUserToDatabase(user: User) {
    if (user) {
      const dbUser: DatabaseUser = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        emailVerified: user.emailVerified,
        isAnonymous: user.isAnonymous,
        photoURL: user.photoURL,
        posts: [],
        boardsFollowing: [],
      };

      this.db
        .collection('users')
        .withConverter(this.userConverter)
        .doc(user.uid)
        .set(dbUser);
    }
  }

  public async getUserFromDatabase(user: User): Promise<DatabaseUser> {
    let dbUser: DatabaseUser = null;

    if (user) {
      await this.db
        .collection('users')
        .withConverter(this.userConverter)
        .doc(user.uid)
        .get()
        .then((user) => {
          dbUser = user.data();
        });
      return dbUser;
    }
    return dbUser;
  }

  public async updateUser(user: DatabaseUser) {
    await this.db
      .collection('users')
      .withConverter(this.userConverter)
      .doc(user.uid)
      .update(user);
  }

  public async checkIfUserExists(uid: string): Promise<boolean> {
    return await this.db
      .collection('users')
      .where('uid', '==', uid)
      .get()
      .then((ref) => {
        if (ref.docs.length > 0) {
          return true;
        } else {
          return false;
        }
      });
  }
}
