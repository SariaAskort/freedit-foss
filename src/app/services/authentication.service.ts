import { UsersService } from './users.service';
import { DatabaseUser } from './../models/databaseUser.interface';
import { User } from './../models/user.interface';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public user$: DatabaseUser;
  public db: firebase.firestore.Firestore;

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private toastController: ToastController,
    private userService: UsersService
  ) {
    this.db = firebase.firestore();

    this.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.userService.getUserFromDatabase(user).then((user) => {
          this.user$ = user;
        });
      }
    });
  }

  public async logout(): Promise<void> {
    try {
      await firebase.auth().signOut();
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async login(email: string, password: string): Promise<DatabaseUser> {
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      let dbUser: DatabaseUser;
      this.userService.getUserFromDatabase(user).then((user) => {
        dbUser = user;
      });

      return dbUser;
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async register(displayName:string, email: string, password: string): Promise<User> {
    try {
      const { user } = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);

      user.updateProfile({
        displayName: displayName,
        photoURL:
          'https://firebasestorage.googleapis.com/v0/b/freedit-b04a6.appspot.com/o/users%2Favatar.png?alt=media&token=286e30fb-2e8b-4a05-b623-01250a72db66',
      });

      this.userService.addUserToDatabase(user);

      return user;
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async loginGoogle(): Promise<User> {
    try {
      const { user } = await firebase
        .auth()
        .signInWithPopup(new firebase.auth.GoogleAuthProvider());

      this.userService.checkIfUserExists(user.uid).then((exists) => {
        if (!exists) {
          this.userService.addUserToDatabase(user);
        }
      });

      return user;
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async loginFacebook(): Promise<User> {
    try {
      const { user } = await firebase
        .auth()
        .signInWithPopup(new firebase.auth.FacebookAuthProvider());

      this.userService.checkIfUserExists(user.uid).then((exists) => {
        if (!exists) {
          this.userService.addUserToDatabase(user);
        }
      });

      return user;
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async loginTwitter(): Promise<User> {
    try {
      const { user } = await firebase
        .auth()
        .signInWithPopup(new firebase.auth.TwitterAuthProvider());

      this.userService.checkIfUserExists(user.uid).then((exists) => {
        if (!exists) {
          this.userService.addUserToDatabase(user);
        }
      });

      return user;
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async signInAnonymously() {
    await firebase.auth().signInAnonymously();
  }

  public resetPassword(email: string) {
    try {
      firebase.auth().sendPasswordResetEmail(email);
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public async sendVerificationEmail() {
    try {
      (await firebase.auth().currentUser).sendEmailVerification();
    } catch (error) {
      this.presentToast(error.message);
    }
  }

  public isEmailVerified(user: User): boolean {
    return user.emailVerified;
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 4000,
    });
    toast.present();
  }
}
