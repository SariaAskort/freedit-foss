import { PostShort } from './../models/post-short.interface';
import { Board } from 'src/app/models/board.interface';
import { ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import { DatabaseUser } from '../models/databaseUser.interface';
import { Post } from '../models/post.interface';
import { BoardsService } from './boards.service';
import { UsersService } from './users.service';
import { User } from '../models/user.interface';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  private user: User;
  private dbUser: DatabaseUser;
  private db: firebase.firestore.Firestore;

  private postConverter = {
    toFirestore: (post: Post) => {
      return {
        id: post.id,
        title: post.title,
        body: post.body,
        imageUrl: post.imageUrl,
        authorUid: post.authorUid,
        authorImageUrl: post.authorImageUrl,
        authorName: post.authorName,
        comments: post.comments,
        karma: post.karma,
        votedByUid: post.votedByUid,
        creationDate: post.creationDate,
        boardId: post.boardId,
      };
    },
    fromFirestore: (snapshot, options) => {
      const data = snapshot.data(options);
      return {
        id: data.id,
        title: data.title,
        body: data.body,
        imageUrl: data.imageUrl,
        authorUid: data.authorUid,
        authorImageUrl: data.authorImageUrl,
        authorName: data.authorName,
        comments: data.comments,
        karma: data.karma,
        votedByUid: data.votedByUid,
        creationDate: data.creationDate,
        boardId: data.boardId,
      };
    },
  };

  constructor(
    private toastController: ToastController,
    private boardService: BoardsService,
    private userService: UsersService,
    private authService: AuthenticationService
  ) {
    this.db = firebase.firestore();
    this.getDatabaseUser();
  }

  private getDatabaseUser() {
    this.authService.afAuth.user.subscribe((user: User) => {
      if (user) {
        this.user = user;
        this.userService.getUserFromDatabase(user).then((dbUser) => {
          this.dbUser = dbUser;
        });
      } else {
        this.user = null;
      }
    });
  }

  public createPost(post: Post, board: Board, file?: File) {
    this.db
      .collection('posts')
      .withConverter(this.postConverter)
      .add(post)
      .then((docRef) => {
        post.id = docRef.id;
        if (file) {
          var uploadTask = firebase
            .storage()
            .ref()
            .child(`posts/${post.id}.jpg`)
            .put(file);

          uploadTask.on(
            'state_changed',
            (snapshot) => {
              var progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              this.presentToast('Upload is ' + progress + '% done');
            },
            (error) => {
              this.presentToast(error.message);
            },
            () => {
              firebase
                .storage()
                .ref()
                .child(`posts/${post.id}.jpg`)
                .getDownloadURL()
                .then((url) => {
                  post.imageUrl = url;
                  this.updatePost(post);
                  const postShort: PostShort = {
                    id: post.id,
                    imageUrl: post.imageUrl,
                    title: post.title,
                  };

                  board.posts.push(postShort);
                  this.boardService.updateBoard(board);

                  this.dbUser.posts.push(postShort);
                  this.userService.updateUser(this.dbUser);
                });
            }
          );
        } else {
          this.updatePost(post);
          const postShort: PostShort = {
            id: post.id,
            title: post.title,
            imageUrl: null,
          };
          board.posts.push(postShort);
          this.boardService.updateBoard(board);
          this.dbUser.posts.push(postShort);
          this.userService.updateUser(this.dbUser);
        }
      });
  }

  public updatePost(post: Post) {
    this.db.collection('posts').doc(post.id).set(post);
  }

  public deletePost(post: Post, board: Board) {
    this.db.collection('posts').doc(post.id).delete();
    const index = board.posts.indexOf({
      id: post.id,
      imageUrl: post.imageUrl,
      title: post.title,
    });
    board.posts.splice(index, 1);
    this.boardService.updateBoard(board);
  }

  public getLatestPostsPaginated(lastDocument?: Post): Post[] {
    let page: Post[] = [];
    if (lastDocument) {
      this.db
        .collection('posts')
        .withConverter(this.postConverter)
        .startAfter(lastDocument)
        .limit(25)
        .orderBy('creationDate')
        .get()
        .then((documents) => {
          for (let i = 0; i < documents.docs.length; i++) {
            const post = documents.docs[i];
            page.push(post.data());
          }
        });
    } else {
      this.db
        .collection('posts')
        .withConverter(this.postConverter)
        .limit(25)
        .orderBy('creationDate')
        .get()
        .then((documents) => {
          for (let i = 0; i < documents.docs.length; i++) {
            const post = documents.docs[i];
            page.push(post.data());
          }
        });
    }
    return page;
  }

  public async getPostById(id: string): Promise<Post> {
    return await this.db
      .collection('posts')
      .withConverter(this.postConverter)
      .doc(id)
      .get()
      .then((doc) => {
        return doc.data();
      });
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
