import { ToastController } from '@ionic/angular';
import { Board } from './../models/board.interface';
import { AuthenticationService } from './authentication.service';
import { DatabaseUser } from './../models/databaseUser.interface';
import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

@Injectable({
  providedIn: 'root',
})
export class BoardsService {
  private user: DatabaseUser;
  public db: firebase.firestore.Firestore;

  private boardConverter = {
    toFirestore: (board: Board) => {
      return {
        id: board.id,
        handle: board.handle,
        name: board.name,
        description: board.description,
        followers: board.followers,
        posts: board.posts,
        adminUid: board.adminUid,
        moderatorsUids: board.moderatorsUids,
        isAnonymous: board.isAnonymous,
        imageUrl: board.imageUrl,
        lastModified: board.lastModified,
      };
    },
    fromFirestore: (snapshot, options) => {
      const data = snapshot.data(options);
      return {
        id: data.id,
        handle: data.handle,
        name: data.name,
        description: data.description,
        followers: data.followers,
        posts: data.posts,
        adminUid: data.adminUid,
        moderatorsUids: data.moderatorsUids,
        isAnonymous: data.isAnonymous,
        imageUrl: data.imageUrl,
        lastModified: data.lastModified,
      };
    },
  };

  constructor(
    private authService: AuthenticationService,
    private toastController: ToastController
  ) {
    this.user = this.authService.user$;

    this.db = firebase.firestore();
  }

  public createBoard(board: Board, file?: File) {
    this.checkIfBoardExists(board.handle)
      .then((exists) => {
        if (exists) {
          throw new Error(`Board with handle ${board.handle} already exists.`);
        } else {
          this.db
            .collection('boards')
            .withConverter(this.boardConverter)
            .add(board)
            .then((docRef) => {
              board.id = docRef.id;
              if (file) {
                var uploadTask = firebase
                  .storage()
                  .ref()
                  .child(`boards/${board.id}.jpg`)
                  .put(file);
                uploadTask.on(
                  'state_changed',
                  (snapshot) => {
                    var progress =
                      (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    this.presentToast('Upload is ' + progress + '% done');
                  },
                  (error) => {
                    this.presentToast(error.message);
                  },
                  () => {
                    firebase
                      .storage()
                      .ref()
                      .child(`boards/${board.id}.jpg`)
                      .getDownloadURL()
                      .then((url) => {
                        board.imageUrl = url;
                        this.updateBoard(board);
                      });
                  }
                );
              } else {
                firebase
                  .storage()
                  .ref()
                  .child(`boards/board.png`)
                  .getDownloadURL()
                  .then((url) => {
                    board.imageUrl = url;
                    this.updateBoard(board);
                  });
              }
            });
        }
      })
      .catch((error) => {
        throw error;
      });
  }

  public async getBoardById(id: string): Promise<Board> {
    return await this.db
      .collection('boards')
      .withConverter(this.boardConverter)
      .doc(id)
      .get()
      .then((document) => {
        return document.data();
      });
  }

  public updateBoard(board: Board) {
    this.db
      .collection('boards')
      .withConverter(this.boardConverter)
      .doc(board.id)
      .update(board);
  }

  private async checkIfBoardExists(handle: string): Promise<boolean> {
    return await this.db
      .collection('boards')
      .where('handle', '==', handle)
      .get()
      .then((ref) => {
        if (ref.docs.length > 0) {
          return true;
        } else {
          return false;
        }
      });
  }

  public getPaginatedBoards(lastDocument?: Board): Board[] {
    let page: Board[] = [];
    if (lastDocument) {
      this.db
        .collection('boards')
        .withConverter(this.boardConverter)
        .startAfter(lastDocument)
        .limit(25)
        .get()
        .then((documents) => {
          for (let i = 0; i < documents.docs.length; i++) {
            const board = documents.docs[i];
            page.push(board.data());
          }
        });
    } else {
      this.db
        .collection('boards')
        .withConverter(this.boardConverter)
        .limit(25)
        .get()
        .then((documents) => {
          for (let i = 0; i < documents.docs.length; i++) {
            const board = documents.docs[i];
            page.push(board.data());
          }
        });
    }
    return page;
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
